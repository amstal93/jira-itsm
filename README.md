## Jira Service Management as an ITSM solution

### (Nihil). Prerequisites
- `docker` 20.10+, `docker-compose` 1.29+

### I. Initial Config
1. With local deployment, we need to add `127.0.0.1 jira.internal` into hosts file:
   ```bash
   echo '127.0.0.1 jira.internal' | sudo tee -a /etc/hosts
   ```

2. With other deployment that requires SSL, simply change docker-compose files by referring `.env`:
   ```properties
   # comment out for local deployment, uncomment when need SSL
   COMPOSE_FILE=compose.yml:compose.ssl.yml
   ```
   Add your cert stuff into `cert/`, with certificate chain as `fullchain1.pem` and private key as `privkey1.pem`.

3. For SSL certificate auto renewal, pls install letsencrypt certbot first: https://certbot.eff.org/docs/install.html, then:
   ```bash
   sudo certbot certonly --standalone -d jira.domain.com -m your@email.com --agree-tos -n
   ```

### II. Step by step
1. Build them up:
   ```bash
   docker-compose up -d && docker-compose logs -f postgres jira
   ```

2. Access the homepage (http://jira.internal), you'll reach the license prompt screen. Copy the license server id, for example `BPOD-YXDV-LMVB-DIC0`, and run command below, with `-p` for product, `-m` for email, `-n` for license name, `-s` for license server id:
   ```bash
   java -jar atlassian-agent.jar \
	-m your@email.com \
	-n hino \
	-o akatsuki \
	-p jsd \
	-s BPOD-YXDV-LMVB-DIC0
   ```

3. Copy the generated license code, get back to your browser and submit it, then BOOM!!! you're done :whale:

4. Tired of Jira, burn them down, but keep the volumes in case you change your mind:
   ```bash
   docker-compose down
   ```

### IV. Frequently Q&A
1. At **step 2**, if **java** is not installed at your machine, use below docker trick:
   ```bash
   docker run --rm -v "${PWD}/atlassian-agent.jar:/atlassian-agent.jar" \
	openjdk:8-jre-alpine \
	java -jar atlassian-agent.jar \
	-m your@email.com -n hino -o akatsuki -p jsd \
	-s BPOD-YXDV-LMVB-DIC0
   ```

